# SimpleGram

SimpleGram adalah aplikasi media sosial sederhana berbasis foto dan deskripsi. Setelah mendaftar menjadi member, pengguna dapat mengupload foto atau gambar serta memberikan deskripsi pada foto tersebut. Selain itu, pengguna juga dapat melihat foto yang telah diupload oleh pengguna lain serta memberikan komentar pada pada postingan foto yang diinginkan.

### Fitur Tersedia

Beberapa fitur yang telah tersedia pada aplikasi ini adalah:

- Register untuk pengguna baru
- Login & Logout
- Posting foto baru
- Mengomentari foto yang ada
- Edit & Hapus pada postingan fotonya sendiri

### Fitur yang saya sudah tambahkan

- Reset password pengguna
- Edit profil pengguna
- Upload foto profil pengguna
- Like dan daftar like
- Bookmark Post

### Fitur yang Belum Tersedia

- Pesan private sederhana

### Deployment di Lokal

Simplegram ini dikembangkan menggunakan Framework Python Flask dengan struktur koding mengikuti pola HMVC.

Untuk instalasi aplikasi di PC / laptop pastikan sebelumnya sudah terinstall Python dan basis data MySQL, kemudian ikuti langkah berikut:

- Clone repositori ini di lokal
- Ubah koneksi basis data pada file `config.py`, sesuaikan dengan basis data yang terinstall
- Selanjutnya jalankan perintah di bawah ini pada terminal
    ```
    pip install -r requirements.txt
    python main.py
    ```
- Untuk keamanan extra, saya menggunakan environment variable untuk menyimpan beberapa data yang sensitive, seperti data password. Untuk menjalankan aplikasi ini di local, maka perlu ditambahkan secara manual beberapa environment seperti dibawah ini :
    ```
    DB_HOST
    DB_USER
    DB_PASS
    MAIL_USERNAME
    MAIL_PASSWORD
    ```
- Untuk menambahkan environment variable tersebut bisa menggunakan command seperti di bawah ini:
    ```
    // untuk bash
    $ export DB_HOST="localhost:3306"

    // untuk fish shell
    set -x DB_HOST localhost:3306

    // untuk powershell
    > $env:DB_HOST = "localhost:3306"
    ```
    [Referensi](https://flask.palletsprojects.com/en/2.0.x/config/#configuring-from-environment-variables)
- Tambahkan semua environment variable diatas
- Apabila anda terjadi error saat reset password, pada file `controllers/auth.py` anda bisa mengubah method `send_random_passwor`d menjadi seperti ini:
    ```
    def send_random_password(user):
        chars = string.ascii_letters + string.digits
        temp_password = ''.join(random.choice(chars) for _ in range(16))
        user.password = generate_password_hash(temp_password)
        db.session.commit()

        print(temp_password)
        # send_email_random_password(user, temp_password)
    ```
    lalu check log untuk mendapatkan password sementara.