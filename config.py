from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from dotenv import load_dotenv, find_dotenv
import os

SECRET_KEY = '4YrzfpQ4kGXjuP6wasdfKJwer*&6qwasd'

DB = {
    'host': os.environ.get('DB_HOST'),
    'user': os.environ.get('DB_USER'),
    'pass': os.environ.get('DB_PASS'),
    'name': 'simplegram'
}

DB_URI = f'mysql+pymysql://{DB["user"]}:{DB["pass"]}@{DB["host"]}/{DB["name"]}?charset=utf8mb4'

db = SQLAlchemy()
login_manager = LoginManager()
login_manager.login_view = 'sg.login'

import simplegram.routes as routes