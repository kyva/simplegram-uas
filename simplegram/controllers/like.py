from flask import redirect, request, flash, jsonify
from flask_login import login_required, current_user

from simplegram.models.models import User, Like
from config import db

def api_likes(post_id):
    likeData = db.session.query(Like, User).join(User)\
                    .filter(Like.post_id==post_id)\
                    .order_by(Like.created_at.desc())\
                    .all()
    likeDict = []
    for like, user in likeData:
        adict = {'id': like.id,
                 'username': user.username,
                 'created_at': like.created_at.strftime('%Y-%m-%d'), 
                 'updated_at': like.updated_at.strftime('%Y-%m-%d')}
        likeDict.append(adict)
    return jsonify(likeDict)

@login_required
def add_like(post_id):
    like = Like(post_id=post_id, user_id=current_user.id)
    db.session.add(like)
    db.session.commit()

    flash({'info': 'Berhasil menambahkan like'})
    return redirect('/')
