from flask import render_template, redirect, request, flash, jsonify
from flask_login import login_required, current_user
from sqlalchemy import text

from simplegram.models.models import User, Bookmark
from config import db

@login_required
def add_bookmark(post_id):
    bookmark = Bookmark(post_id=post_id, user_id=current_user.id)
    db.session.add(bookmark)
    db.session.commit()

    flash({'info': 'Berhasil menambahkan post ke dalam Bookmark'})
    return redirect('/')

@login_required
def remove_bookmark(post_id):
    bookmark = Bookmark.query.filter_by(post_id=post_id, user_id=current_user.id).first()

    if bookmark:
        db.session.delete(bookmark)
        db.session.commit()
        flash({'info': 'Berhasil menghapus post dari bookmark'})
    else:
        flash({'info': 'Bookmark tidak ditemukan'})

    return redirect('/mybookmark')

@login_required
def my_bookmark():
    sql = '''select 
            post.id, post.user_id, image, post.description, 
            post.created_at, post.updated_at, username, fullname, profpic,  coalesce(lk.count_like, 0) as count_like,
            (select count(*) from comment c where c.post_id = post.id) as jml_comment,
            (select count(*) from likes where likes.post_id = post.id) as jml_likes
        from post 
        join user on (post.user_id = user.id)
        join bookmarks on bookmarks.user_id = :current_user_id and bookmarks.post_id = post.id
        left join (select count(likes.id) as count_like, likes.post_id from likes where user_id = :current_user_id group by likes.post_id) as lk
            on lk.post_id = post.id
        order by post.created_at desc
    '''

    postData = db.session.execute(text(sql), {'current_user_id': current_user.id}).all()
    return render_template('my_bookmarks.html', postData=postData)
