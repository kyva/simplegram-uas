from flask import render_template, redirect, flash, request, Flask, url_for
from simplegram.forms import LoginForm, RegisterForm, ProfileForm, ResetPasswordForm
from simplegram.models.models import User
from config import db
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from flask_login import login_user, logout_user, current_user, login_required
from flask_mail import Mail, Message
from simplegram import SG
from dotenv import load_dotenv, find_dotenv
import random, string, os

def login():
    if current_user.is_authenticated:
        return redirect('/')

    loginForm = LoginForm()
    return render_template('login.html', form=loginForm)

def login_post():
    # from bootstrap import bcrypt
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user: 
            if check_password_hash(user.password, form.password.data):
                login_user(user, remember=form.remember.data)
                return redirect('/')
            else:
                flash({'error': 'Username & Password tidak sesuai'})
        else:
            flash({'error': 'Username & Password tidak sesuai'})
    
    return render_template('login.html', form=form)

def register():
    if current_user.is_authenticated:
        return redirect('/')
    
    form = RegisterForm()
    return render_template('register.html', form=form)

def register_post():
    # from bootstrap import bcrypt
    form = RegisterForm()
    if form.validate_on_submit():
        safe_pass = generate_password_hash(form.password.data)
        newUser = User(username=form.username.data, email=form.email.data, 
                       password=safe_pass, fullname=form.fullname.data)
        db.session.add(newUser)
        db.session.commit()
        return redirect('/login')
    return render_template('register.html', form=form)

def reset_password():
    form = ResetPasswordForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            email = form.email.data
            user = User.query.filter_by(email=email).first()

            if user:
                send_random_password(user)
                return redirect(url_for('sg.login'))
            else:
                flash({'error': 'Terjadi kesalahan, periksa kembali email anda'})

    return render_template('reset_password.html', form=form)

def send_random_password(user):
    chars = string.ascii_letters + string.digits
    temp_password = ''.join(random.choice(chars) for _ in range(16))
    user.password = generate_password_hash(temp_password)
    db.session.commit()

    send_email_random_password(user, temp_password)

def send_email_random_password(user, temp_password):
    app = Flask(__name__)
    app.config['MAIL_SERVER'] = 'smtp.gmail.com'
    app.config['MAIL_PORT'] = 465
    app.config['MAIL_USE_TLS'] = False
    app.config['MAIL_USE_SSL'] = True
    app.config['MAIL_USERNAME'] = os.environ.get('MAIL_USERNAME')
    app.config['MAIL_PASSWORD'] = os.environ.get('MAIL_PASSWORD')

    mail = Mail(app)

    message = Message('Password Sementara', sender='admin@simplegram.com', recipients=[user.email])
    message.body = f'''Password sementara anda adalah: {temp_password}
    Mohon login menggunakan password di atas, kemudian ubah password anda.
    '''

    mail.send(message)


@login_required
def my_profile():
    form = ProfileForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            user = User.query.get(current_user.id)
            if form.description and form.description.data:
                user.description = form.description.data

            if form.profpic and form.profpic.data:
                profpic_filename = secure_filename(form.profpic.data.filename)
                upload_dir = os.path.join(os.path.dirname(SG.root_path), 'static', 'images', 'profpics')
                os.makedirs(upload_dir, exist_ok=True)

                form.profpic.data.save(os.path.join(upload_dir, profpic_filename))
                user.profpic = os.path.join('images', 'profpics', profpic_filename)

            if form.password and form.password.data:
                password = generate_password_hash(form.password.data)
                user.password = password

            db.session.commit()
        else:
            form.fullname.data = current_user.fullname
            form.profpic.data = current_user.profpic
            form.description.data = current_user.description

    return render_template('profil.html', form=form, )

def logout():
    logout_user()
    return redirect('/login')