from simplegram import SG

from simplegram.controllers.auth import *
from simplegram.controllers.post import *
from simplegram.controllers.like import *
from simplegram.controllers.bookmark import *

SG.route('/')(index)
SG.route('/addpost', methods=['GET', 'POST'])(add_post)
SG.route('/editpost/<post_id>', methods=['GET', 'POST'])(edit_post)
SG.route('/delpost/<post_id>')(del_post)
SG.route('/api/comments/<post_id>')(api_comments)
SG.route('/api/likes/<post_id>')(api_likes)

SG.route('/addcomment/<post_id>', methods=['GET', 'POST'])(add_comment)
SG.route('/addlike/<post_id>', methods=['GET'])(add_like)
SG.route('/addbookmark/<post_id>', methods=['GET'])(add_bookmark)
SG.route('/removebookmark/<post_id>', methods=['GET'])(remove_bookmark)

SG.route('/login')(login)
SG.route('/login', methods=['POST'])(login_post)
SG.route('/logout')(logout)
SG.route('/register')(register)
SG.route('/register', methods=['POST'])(register_post)
SG.route('/myprofile', methods=['GET', 'POST'])(my_profile)
SG.route('/mypost')(my_post)
SG.route('/mybookmark')(my_bookmark)
SG.route('/reset_password', methods=['GET', 'POST'])(reset_password)
